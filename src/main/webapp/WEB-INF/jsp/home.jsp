<%--
  Created by IntelliJ IDEA.
  User: saishav7
  Date: 30/03/2016
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Stock Prices</title>
</head>
<body>

    Stock Code: <input id = "code" type="text" name="code"><br>
    Amount: <input id = "amount" type="text" name="amount"><br>
    <input name="button" type="button" value=" Search "
    onClick="location.href=('?code='+document.getElementById('code').value+'&amount='
    +document.getElementById('amount').value);return false;" />
    <br/>
    <c:if test="${not empty totalAmount}">
        Total Amount: $<c:out value='${totalAmount}'/>
    </c:if>


</body>
</html>
