
CREATE ROLE postgres LOGIN
  ENCRYPTED PASSWORD 'md53175bce1d3201d16594cebf9d7eb3f9d'
  SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

CREATE TABLE public.price
(
  id bigint NOT NULL,
  stockcode character varying(50),
  price numeric,
  CONSTRAINT price_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.price
  OWNER TO postgres;


CREATE SEQUENCE hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 146714
  CACHE 1;
ALTER TABLE hibernate_sequence
  OWNER TO postgres;
