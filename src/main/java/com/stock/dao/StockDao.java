package com.stock.dao;

import com.stock.model.Stock;

/**
 * Created by saishav7 on 30/03/2016.
 */
public interface StockDao extends GenericDao<Stock> {

}
