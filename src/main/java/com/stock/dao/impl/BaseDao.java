package com.stock.dao.impl;

import javax.sql.DataSource;

/**
 * Created by saishav7 on 30/03/2016.
 */
public abstract class BaseDao {

    protected DataSource dataSource;
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
