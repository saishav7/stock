package com.stock.dao.impl;

import com.stock.dao.QueryUpdateHelper;
import com.stock.dao.StockDao;
import com.stock.model.Stock;

import javax.sql.DataSource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by saishav7 on 30/03/2016.
 */
public class StockDaoImpl extends BaseDao implements StockDao {


    public Stock findById(Serializable id) {
        return null;
    }

    public Serializable save(Stock stock) {
        String sql = "INSERT INTO public.price(id, stockcode, price) " +
                " VALUES (nextval('hibernate_sequence'), ?, ?); ";
        ArrayList<Object> params = new ArrayList<Object>();

        params.add(stock.getCode());
        params.add(new BigDecimal(stock.getPrice()));

        QueryUpdateHelper.update(dataSource, sql, params);

        return null;
    }

    public List<Stock> findByIds(List<? extends Serializable> ids) {
        return null;
    }

    public void update(Stock domain) {

    }

    public void delete(Stock domain) {

    }

}
