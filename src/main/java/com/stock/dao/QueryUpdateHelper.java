package com.stock.dao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by saishav7 on 31/03/2016.
 */
public class QueryUpdateHelper {

    public static void update(DataSource dataSource, String sql, ArrayList<Object> params) {
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);

            for (int i = 0; i < params.size(); i++) {
                Object o = params.get(i);
                stmt.setObject(i+1, o);
            }

            stmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
}
