package com.stock.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saishav7 on 30/03/2016.
 */
public interface GenericDao<T> {

    T findById(Serializable id);

    Serializable save(T domain);

    List<T> findByIds(List<? extends Serializable> ids);

    void update(T domain);

    void delete(T domain);
}
