package com.stock.model;

import lombok.Data;

/**
 * Created by saishav7 on 30/03/2016.
 */

@Data
public class Stock {

    String code;
    String amount;
    String price;

}
