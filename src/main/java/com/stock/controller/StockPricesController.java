package com.stock.controller;

import com.stock.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.stock.service.StockPricesService;

import java.math.BigDecimal;


/**
 * Created by saishav7 on 30/03/2016.
 */

@Controller
public class StockPricesController {

    @Autowired
    private StockPricesService stockService;

   @RequestMapping("/")
   public ModelAndView getStockDetails(@RequestParam(required = false, value = "code") String stockCode,
                                       @RequestParam(required = false, value = "amount") String stockAmount) {

       ModelAndView mav = new ModelAndView();
       mav.setViewName("home");

        if(stockCode == null || stockAmount == null) {
            return mav;
        }

       Stock stock = new Stock();
       stock.setCode(stockCode);
       stock.setAmount(stockAmount);

       try {
           String price = stockService.getPrice(stockCode);
           stock.setPrice(price);
           stockService.saveNewStock(stock);
       } catch (Exception e) {
           System.out.print("Could not save data" + e);
       }

       String priceValue = stock.getPrice();

       if(priceValue != null) {
         try {
             BigDecimal totalAmount = stockService.getTotalAmount(priceValue, stockAmount);
             mav.addObject("totalAmount", totalAmount);
         } catch(Exception e) {
             System.out.print("Error occoured while calcualating total amount" + e);
             mav.addObject("totalAmount", "Unable to calculate");
         }
       } else {
           mav.addObject("totalAmount", "Unable to calculate");
       }

       return mav;

   }

}
