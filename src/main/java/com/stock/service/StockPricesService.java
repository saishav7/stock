package com.stock.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.dao.StockDao;
import com.stock.model.Stock;
import com.stock.model.yahoo.MarketDetail;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;

/**
 * Created by saishav7 on 30/03/2016.
 */

@Component
public class StockPricesService {

    @Autowired
    private StockDao stockDao;

    @Transactional
    public void saveNewStock(Stock stock) {
        stockDao.save(stock);
    }

    public static BigDecimal getTotalAmount(String priceValue, String stockAmount) {

        BigDecimal price = new BigDecimal(priceValue);
        BigDecimal amount = new BigDecimal(stockAmount);
        BigDecimal totalAmount = price.multiply(amount);
        return totalAmount;
    }

    public String getPrice(String stockCode) throws IOException, JSONException {
        String url = "http://finance.yahoo.com/webservice/v1/symbols/" + stockCode + "/quote?format=json";
        MarketDetail marketDetails;
        try {
            marketDetails  = callAPI(url, MarketDetail.class);
        } catch (Exception e) {
            return null;
        }
        String price = marketDetails.getList().getResources().get(0).getResource().getFields().getPrice();
        return price;
    }

    public <T> T callAPI(String url, Class<T> clazz) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        HttpResponse httpResponse = null;
        HttpClient httpClient = new DefaultHttpClient();

        try {
            URI uri = new URI(url);
            HttpGet httpRequest = new HttpGet(uri);
            httpRequest.addHeader("Accept", "application/json");
            httpRequest.addHeader("Content-Type", "application/json");
            httpResponse = httpClient.execute(httpRequest);

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = responseHandler.handleResponse(httpResponse);
            if(responseBody == null) {
                return null;
            }
            T response = null;
            try {
                response = mapper.readValue(responseBody, clazz);
                if (response == null) {
                    throw new Exception("Cannot parse the response");
                }

            } catch (Exception e) {
                throw new Exception("Cannot parse the response", e);
            }
            return response;
        } finally {
            if (httpResponse != null) {
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    entity.consumeContent();
                }
            }
            if (httpClient != null) {
                httpClient.getConnectionManager().shutdown();
            }
        }
    }


}
